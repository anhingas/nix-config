{ pkgs
, config
, ...
}:
let
  ifExists = groups: builtins.filter (group: builtins.hasAttr group config.users.groups) groups;
in
{
  users.users.joe = {
    hashedPassword = "$y$j9T$h/hw.aiwCewxYqcQZrj0j/$.Z8G1p2BfVDRHS6Pee4PciEXXhSHLFbUlyBVoPzg3v4";
    isNormalUser = true;
    shell = pkgs.zsh;
    extraGroups =
      [
        "audio"
        "networkmanager"
        "users"
        "video"
        "wheel"
      ]
      ++ ifExists [
        "docker"
        "plugdev"
        "render"
        "lxd"
      ];

    openssh.authorizedKeys.keys = [
      "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIEmCKT5Ig6KypKwP+F9L2bizDGzri/IRMMQiU9DWVkpO hydrogen"
    ];

    packages = [ pkgs.home-manager ];
  };

  # This is a workaround for not seemingly being able to set $EDITOR in home-manager
  environment.sessionVariables = {
    EDITOR = "vim";
  };
}
