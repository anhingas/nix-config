{
  description = "joe's nixos configuration";
  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/nixos-23.05";
    nixpkgs-unstable.url = "github:nixos/nixpkgs/nixos-unstable";
    nixos-hardware.url = "github:NixOS/nixos-hardware/master";

    agenix.url = "github:ryantm/agenix";
    agenix.inputs.nixpkgs.follows = "nixpkgs-unstable";

    nix-formatter-pack.url = "github:Gerschtli/nix-formatter-pack";
    nix-formatter-pack.inputs.nixpkgs.follows = "nixpkgs-unstable";

    hyprland.url = "github:hyprwm/Hyprland";
    hyprland-contrib.url = "github:hyprwm/contrib";

    sf-pro-fonts-src.url = "github:jnsgruk/sf-pro-fonts";
    sf-pro-fonts-src.flake = false;

    disko.url = "github:nix-community/disko";
    disko.inputs.nixpkgs.follows = "nixpkgs-unstable";

    vscode-server.url = "github:nix-community/nixos-vscode-server";
    vscode-server.inputs.nixpkgs.follows = "nixpkgs-unstable";

    home-manager.url = "github:nix-community/home-manager";
    home-manager.inputs.nixpkgs.follows = "nixpkgs-unstable";

  };

  outputs =
    { self
    , nixpkgs
    , nixpkgs-unstable
    , nix-formatter-pack
    , ...
    } @ inputs:
    let
      inherit (self) outputs;
      stateVersion = "23.05";
      username = "joe";

      libx = import ./lib { inherit inputs outputs stateVersion username; };
    in
    {
      # nix build .#homeConfigurations."joe@carbon".activationPackage
      homeConfigurations = {
        # Desktop machines
        "${username}@carbon" = libx.mkHome { hostname = "carbon"; desktop = "hyprland"; };
        "${username}@hydrogen" = libx.mkHome { hostname = "hydrogen"; desktop = "hyprland"; };
        "${username}@oxygen" = libx.mkHome { hostname = "oxygen"; desktop = "hyprland"; };
        # Headless machines
        "${username}@nitrogen" = libx.mkHome { hostname = "nitrogen"; };
        "${username}@helium" = libx.mkHome { hostname = "helium"; };
        "ubuntu@dev" = libx.mkHome { hostname = "dev"; user = "ubuntu"; };
      };

      # nix build .#nixosConfigurations.carbon.config.system.build.toplevel
      nixosConfigurations = {
        # Desktop machines
        carbon = libx.mkHost { hostname = "carbon"; desktop = "hyprland"; };
        hydrogen = libx.mkHost { hostname = "hydrogen"; desktop = "hyprland"; };
        oxygen = libx.mkHost { hostname = "oxygen"; desktop = "hyprland"; };
        # Headless machines
        nitrogen = libx.mkHost { hostname = "nitrogen"; pkgsInput = nixpkgs; };
        helium = libx.mkHost { hostname = "helium"; pkgsInput = nixpkgs; };
      };

      # Custom packages; acessible via 'nix build', 'nix shell', etc
      packages = libx.forAllSystems (system:
        let pkgs = nixpkgs-unstable.legacyPackages.${system};
        in import ./pkgs { inherit pkgs; }
      );

      # Custom overlays
      overlays = import ./overlays { inherit inputs; };

      # Devshell for bootstrapping
      # Accessible via 'nix develop' or 'nix-shell' (legacy)
      devShells = libx.forAllSystems (system:
        let pkgs = nixpkgs-unstable.legacyPackages.${system};
        in import ./shell.nix { inherit pkgs; }
      );

      formatter = libx.forAllSystems (system:
        nix-formatter-pack.lib.mkFormatter {
          pkgs = nixpkgs-unstable.legacyPackages.${system};
          config.tools = {
            deadnix.enable = true;
            nixpkgs-fmt.enable = true;
            statix.enable = true;
          };
        }
      );

      nixConfig = {
        substituters = [
          "https://cache.nixos.org"
          "https://hyprland.cachix.org"
        ];
        # nix community's cache server
        extra-substituters = [
          "https://nix-community.cachix.org"
          "https://nixpkgs-wayland.cachix.org"
        ];
        trusted-public-keys = [
          "cache.nixos.org-1:6NCHdD59X431o0gWypbMrAURkbJ16ZPMQFGspcDShjY="
          "hyprland.cachix.org-1:a7pgxzMz7+chwVL3/pzj6jIBMioiJM7ypFP8PwtkuGc="
          "nix-community.cachix.org-1:mB9FSh9qf2dCimDSUo8Zy7bkq5CX+/rkCWyvRCYg3Fs="
          "nixpkgs-wayland.cachix.org-1:3lwxaILxMRkVhehr5StQprHdEo4IrE8sRho9R9HOLYA="
        ];
      };
    };
}
