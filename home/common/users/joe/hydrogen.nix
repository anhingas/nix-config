_: {
  services.syncthing = {
    enable = true;
    extraOptions = [
      "-gui-address=hydrogen.tailnet-d5da.ts.net:8384"
      "-home=/home/joe/data/.syncthing"
    ];
  };
}
