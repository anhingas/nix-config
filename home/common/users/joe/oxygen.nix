_: {
  services.syncthing = {
    enable = true;
    extraOptions = [
      "-gui-address=oxygen.tailnet-d5da.ts.net:8384"
      "-home=/home/joe/data/.syncthing"
    ];
  };
}
