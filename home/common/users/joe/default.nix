{ inputs, lib, hostname, ... }: {
  imports = [
    inputs.vscode-server.nixosModules.home
  ]
  ++ lib.optional (builtins.pathExists (./. + "/${hostname}.nix")) ./${hostname}.nix;

  services = {
    # Following install, you may need to run:
    # systemctl --user enable auto-fix-vscode-server.service
    # systemctl --user start auto-fix-vscode-server.service
    vscode-server.enable = true;
  };
   home = {
   	  file.".face".source = ./face.png;
   	  file.".ssh/config".text = "
   	    Host github.com
   	      HostName github.com
   	      User git
   	  
   	    
   	  ";
   	  file."Quickemu/nixos-console.conf".text = ''
   	    #!/run/current-system/sw/bin/quickemu --vm
   	    guest_os="linux"
   	    disk_img="nixos-console/disk.qcow2"
   	    disk_size="96G"
   	    iso="nixos-console/nixos.iso"
   	  '';
   	  file."Quickemu/nixos-desktop.conf".text = ''
   	    #!/run/current-system/sw/bin/quickemu --vm
   	    guest_os="linux"
   	    disk_img="nixos-desktop/disk.qcow2"
   	    disk_size="96G"
   	    iso="nixos-desktop/nixos.iso"
   	  '';
   };
}
