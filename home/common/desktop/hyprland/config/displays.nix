_: {
  hydrogen =
    {
      workspace = [
        "1, monitor: HDMI-A-0"
        "2, monitor: HDMI-A-0"
        "3, monitor: DisplayPort-0"
        "4, monitor: DisplayPort-0"
        "5, monitor: HDMI-A-0"
        "6, monitor: HDMI-A-0"
        "7, monitor: HDMI-A-0"
      ];

      monitor = [
        "HDMI-A-0, preferred, 3840x0, 1"
        "DisplayPort-0, preferred, 0x0, 1"
      ];
    };

  carbon = {
    workspace = [ ];
    monitor = [ "eDP-1, preferred, auto, 1" ];
  };
  oxygen = {
    workspace = [ ];
    monitor = [ "eDP-1, preferred, auto, 1" ];
  };
}
