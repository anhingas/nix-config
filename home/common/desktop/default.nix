{ pkgs, desktop, ... }: {
  imports = [
    (./. + "/${desktop}")

    ../dev

    ./alacritty.nix
    ./gtk.nix
    ./qt.nix
    ./xdg.nix
    ./zathura.nix
  ];

  programs = {
    firefox.enable = true;
    mpv.enable = true;
    feh.enable = true;
  };

  home.packages = with pkgs; [
    catppuccin-gtk
    desktop-file-utils
    element-desktop
    google-chrome
    imlib2Full
    libnotify
    logseq
    pamixer
    pavucontrol
    rambox
    signal-desktop
    telegram-cli
    telegram-desktop
    xdg-utils
    xorg.xlsclients
  ];

  fonts.fontconfig.enable = true;
}
